# PTR_UWS_B1



## Co to jest?

Urządzenie będące przebudowaną i rozbudowaną wersją UWSa Jaskiniowca. Główne cechy to
- wykrywanie przechylenia rakiety na podstawie pomiaru pola magnetycznego Ziemi
- regulacja kąta zadziałania poprzez potencjometr
- ciągłość zapalnika sygnalizowana jest na 2 sposoby
    - dioda LED zielona przy zapalniku - zapalona = zapalnik ciągły
    - buzzer - dźwięk jeśli zapalnik ciągły i przechył większy niż zadany
- zworka do uzbrajania układu (zapewnia bezpieczeństwo)
- jeśli zworka ARM jest rozłączona to buzzer służy do weryfikacji ustawienia kąta wyzwolenia
- ogniwo Li-pol 1s 80mAh marki Turnigy
- wbudowane ładowanie z USB i sygnalizacja poziomu akumulatora


## Co zawiera repozytorium?

- [X] Schematy wszystkich dotychczasowych rewizji

## Foto góry płytki

![TOP](https://gitlab.com/space.tech/ptr_uws_b/-/raw/main/foto/UWS_B1_top.jpg)



